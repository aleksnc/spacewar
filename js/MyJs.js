const getRandom = (min, max) => {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}


$(document).ready(function () {
    $(document).on('click', '.firststart', function () {
        $('.startPage').hide();
        defaultMusicVolume = localStorage.getItem('musicVolume') || 1;
        defaultSfxVolume = localStorage.getItem('sfxVolume') || 1;
        savedShipIndex = localStorage.getItem('savedIndexShip') * 1;

        hiScore = localStorage.getItem('hiScore') || 0;
        superstart({
            defaultMusicVolume,
            defaultSfxVolume,
            hiScore,
            savedShipIndex
        })
    })
})

// let restart = document.querySelector('.restart');

// restart.onclick = () => {
//     superstart()
// }

const pjs = new PointJS('2D', 720 / 2, 1280 / 2, { // 16:9
    backgroundColor: 'transparent' // if need
});


let game = pjs.game;

let backgroundAudio = new Audio('./sound/sound.wav');

function superstart({
    ufoMaxPosX = 5,
    startSpeed = 0.7,
    defaultHelth = 100,
    defaultMusicVolume = 1,
    defaultSfxVolume = 1,
    hiScore = 0,
    savedShipIndex = 0
} = {}) {

    backgroundAudio.addEventListener('ended', function () {
        this.currentTime = 0;
        this.play();
    }, false);
    backgroundAudio.play();
    backgroundAudio.volume = defaultMusicVolume / 10;


    let speed = startSpeed;
    let helth = defaultHelth;
    let spaceShips = [];
    let score = 0;
    let viewHiScore = hiScore;
    let iterationCreateShip = 0;
    let iterationMoveBg = 0;
    let iterationStepPlayer = 0;
    let bombArr = [];
    let loose = false;
    let MovePlayer;

    pjs.system.initFullPage();
    pjs.system.initFPSCheck();
    touch = pjs.touchControl.initTouchControl();

    let size = game.getWH().w / 6;

    let posY = game.getWH().h - size;
    let posX = game.getWH().w - size;

    let menu = game.newRectObject({
        x: 0,
        y: 0,
        w: game.getWH().w,
        h: 50,
        fillColor: "black"
    });

    let lineHelth = game.newRectObject({
        x: 10,
        y: 5,
        w: game.getWH().w / 3,
        h: 20,
        fillColor: "green"
    });

    let buttonBurn = game.newImageObject({
        file: 'image/burn.png',
        w: size - 1,
        h: size - 1,
        y: posY - 20,
        x: posX - 10,
        alpha: 0.5
    });


    /*PLAYER*/
    let playerShip = game.newImageObject({
        file: `image/playership_${savedShipIndex + 1}.png`,
        w: size - 1,
        h: size - 1,
        y: posY - size - 20,
        x: 0
    });

    /*other*/
    function CreateSpaceShip() {
        this.positionX = getRandom(0, ufoMaxPosX) * size;
        this.ship = game.newImageObject({
            file: 'image/spaceship.png',
            w: size - 1,
            h: size - 1,
            y: -size,
            x: this.positionX
        });
        // this.pole = game.newImageObject({
        //     file: 'image/pole.png',
        //     w: size + 9,
        //     h: size + 1,
        //     y: -size,
        //     x: this.positionX - 4,
        // });
        this.health = 0
    }


    function CreateBigSpaceShip() {
        this.ship = game.newImageObject({
            file: 'image/bigShip.png',
            w: size + 31,
            h: size - 1,
            y: -size,
            x: getRandom(0, ufoMaxPosX - 1) * size + 31,
        });
        this.health = 2
    }


    spaceShips.push(new CreateSpaceShip());

    function CreateBomb() {
        this.bomb = game.newImageObject({
            file: 'image/bomb.png',
            w: 16,
            h: 16,
            y: posY - 2 * size,
        });
    }

    let bangImg = pjs.tiles.newAnimation('image/explodem.png', 60, 60, 10);

    let bangAnim = game.newAnimationObject({
        animation: bangImg,
        w: size - 1, h: size - 1,
        x: -size, y: -size
    });

    let bangMilk = bangAnim;

    /*player move*/

    pjs.game.newLoop("myGame", function () {
        // Очистка прошлого кадра отрисовки
        pjs.game.clear();

        $('.bg__wrapper').css(
            'background-position-y', iterationMoveBg++ + 'px'
        )

        if (iterationCreateShip < 120 / speed) {
            iterationCreateShip++;
        } else {
            let type = getRandom(0, 4);


            let ship = new CreateBigSpaceShip();

            if (type < 4) {
                ship = new CreateSpaceShip();
            }


            spaceShips.push(ship);
            iterationCreateShip = 0;
        }

        playerShip.draw();

        for (let s = 0; s < spaceShips.length; s++) {
            Newship = spaceShips[s].ship;
            Newship.draw();
            Newship.y += speed;

            if (playerShip.isIntersect(Newship)) {
                loose = true;
                // game.stop();
            }

            if (Newship.y > game.getWH().h) {
                helth -= 4;

                if (helth <= 0) {
                    loose = true;

                    // game.stop();
                }

                lineHelth.w = (game.getWH().w / 3) * (helth / 100);

                if (helth <= 75) {
                    lineHelth.fillColor = 'yellow'
                }

                if (helth <= 50) {
                    lineHelth.fillColor = 'orange'
                }


                if (helth <= 25) {
                    lineHelth.fillColor = 'red'
                }

                spaceShips.splice(s, 1);
            }
        }

        let attak = pjs.touchControl.isPeekStatic(buttonBurn.getStaticBox());

        let up = pjs.touchControl.isUp();

        if (up == true) {
            buttonBurn.alpha = 0.5;
        }

        MovePlayer = pjs.touchControl.getVector().x;

        if (attak == true) {
            buttonBurn.alpha = 0.8;
            let temp = new CreateBomb();
            temp.bomb.x = playerShip.x + size / 2 - 8;
            bombArr.push(temp);
            let audioObj = new Audio('./sound/piu.mp3');
            audioObj.volume = defaultSfxVolume / 10;
            audioObj.play()
        }

        for (let a = 0; a < bombArr.length; a++) {
            NewBomb = bombArr[a].bomb;
            NewBomb.draw();
            NewBomb.y -= 2;

            for (let s = 0; s < spaceShips.length; s++) {
                Newship = spaceShips[s].ship;
                let healthShip = spaceShips[s].health;

                if (NewBomb.isIntersect(Newship)) {
                    bangAnim.x = Newship.x;
                    bangAnim.y = Newship.y;
                    bangAnim.frame = 0;
                    score += 100;
                    if (score % 100 == 0) {
                        speed += 0.02;
                    }

                    spaceShips[s].health = healthShip - 1;

                    if (healthShip == 0) {
                        spaceShips.splice(s, 1);
                    }

                    let audioObj = new Audio('./sound/bah.mp3');
                    audioObj.volume = defaultSfxVolume / 10;
                    audioObj.play()

                    bombArr.splice(a, 1);
                }
            }
            if (NewBomb.y < 100) {
                bangMilk.x = NewBomb.x;
                bangMilk.y = NewBomb.y;
                bangMilk.frame = 0;

                let audioObj = new Audio('./sound/bahMilk.mp3');
                audioObj.volume = defaultSfxVolume / 10;
                audioObj.play()

                bombArr.splice(a, 1);
            }
        }

        if (bangAnim.frame < 9) {
            bangAnim.draw();
        }
        if (bangMilk.frame < 9) {
            bangMilk.draw();
        }


        if (MovePlayer != 0) {
            iterationStepPlayer++;

            if (iterationStepPlayer < 2) {
                playerShip.x += MovePlayer * size;
            }

            if (iterationStepPlayer > 20) {
                iterationStepPlayer = 0;
            }
        } else {
            iterationStepPlayer = 0;
        }

        if (playerShip.x <= 0) {
            playerShip.x = 0;
        }

        if (playerShip.x >= posX) {
            playerShip.x = posX;
        }

        menu.draw();
        lineHelth.draw();
        buttonBurn.draw();

        if (score > viewHiScore) {
            viewHiScore = score;
        }

        pjs.brush.drawText({
            text: 'SCORE: ' + score,
            color: "white",
            size: 20,
            align: 'right',
            x: game.getWH().w - 10,
            y: 30
        });

        pjs.brush.drawText({
            text: 'HI SCORE: ' + viewHiScore,
            color: "white",
            size: 20,
            align: 'right',
            x: game.getWH().w - 10,
            y: 5
        });

        if (loose) {
            backgroundAudio.pause();
            backgroundAudio.currentTime = 0;

            localStorage.setItem('hiScore', score);

            replay(size, posY, posX)


            // $('.gameover').show();
            // $('.score').empty();
            let coefHelth = helth;
            if (helth == 0) {
                coefHelth = 1;
            }
            // $('.score').html(score);
            // $('.helth').html(helth);
            // $('.total').html(score * coefHelth);
        }

    });


    pjs.game.startLoop("myGame");
}


function replay(size, posY, posX) {
    let playBtn = game.newImageObject({
        file: 'image/button.png',
        w: size - 1,
        h: size - 1,
        y: posY - 0,
        x: posX - 10,
        alpha: 1
    });
    pjs.game.newLoop("menu", function () {
        // Очистка прошлого кадра отрисовки
        pjs.game.clear();

        playBtn.draw();

        let replay = pjs.touchControl.isPeekStatic(playBtn.getStaticBox());

        if (replay) {
            // superstart()
            window.location.replace('./store.html')
        }
    })

    pjs.game.startLoop("menu");
}