const getRandom = (min, max) => {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}


$(document).ready(function () {
    defaultMusicVolume = localStorage.getItem('musicVolume') || 1;
    defaultSfxVolume = localStorage.getItem('sfxVolume') || 1;
    savedIndexShip = localStorage.getItem('savedIndexShip') * 1 || 0;
    startSetting({
        defaultMusicVolume,
        defaultSfxVolume,
        savedIndexShip
    })
})


const pjs = new PointJS('2D', 720 / 2, 1280 / 2, { // 16:9
    backgroundColor: 'transparent' // if need
});


let game = pjs.game;

let backgroundAudio = new Audio('./sound/sound_4_setting.mp3');
let sfxAudio = new Audio('./sound/bah.mp3');

function startSetting({
    defaultMusicVolume = 1,
    defaultSfxVolume = 1,
    savedIndexShip = 0
} = {}) {
    console.log(`savedIndexShip`, savedIndexShip)

    pjs.system.initFullPage();
    pjs.system.initFPSCheck();
    touch = pjs.touchControl.initTouchControl();

    let size = game.getWH().w / 6;

    let posY = game.getWH().h - size;
    let posX = game.getWH().w - size;

    let okBtn = game.newImageObject({
        file: 'image/ok.png',
        w: size * 2,
        h: size - 1,
        y: posY - 25,
        x: posX / 2 - size / 2
    });

    function CreatePlayerShip(posX, posY) {
        this.playerShipArray = [
            'playership_1.png',
            'playership_2.png',
            'playership_3.png'
        ]

        let x = posX + size;
        let y = posY;

        this.createShip = () => {
            let arrShip = [];
            for (let index = 0; index < this.playerShipArray.length; index++) {
                let playerShip = game.newImageObject({
                    file: `image/${this.playerShipArray[index]}`,
                    w: size - 1,
                    h: size - 1,
                    y,
                    x
                });
                x += size + 10;
                // y -= 2;
                arrShip.push(playerShip);
            }
            return arrShip
        }

        this.border = game.newRectObject({
            x,
            y,
            h: size,
            w: size,
            // radius: 3,
            fillColor: "#9ca3bf"
        });
    }

    function CreateVolumeBar(posX, posY) {
        let x = posX;
        let y = posY;
        let h = 30;
        this.initBar = () => {
            let arrBox = [];
            for (let index = 0; index < 11; index++) {
                let box = game.newRectObject({
                    x,
                    y,
                    h,
                    w: 7,
                    // radius: 3,
                    fillColor: "#9ca3bf"
                });
                h += 2;
                x += 8;
                y -= 2;
                arrBox.push(box);
            }
            return arrBox
        }
    }


    let arraySoundBar = new CreateVolumeBar(205, 80).initBar();
    let clickSoundIndex = defaultMusicVolume;

    let arraySfxBar = new CreateVolumeBar(205, 160).initBar();
    let clickSfxIndex = defaultSfxVolume;


    let arrayPlayerShip = new CreatePlayerShip(15, 250).createShip();
    let borederShip = new CreatePlayerShip(15, 250).border;
    let clickShipIndex = savedIndexShip

    const onChangeVolume = (e) => {
        localStorage.setItem('musicVolume', e);
        sfxAudio.pause();
        sfxAudio.currentTime = 0;
        clickSoundIndex = e;
        backgroundAudio.currentTime = 0;
        backgroundAudio.play();

        backgroundAudio.volume = e * 0.1;
    }

    const onChangeSfxVolume = (e) => {
        localStorage.setItem('sfxVolume', e);
        backgroundAudio.pause();
        backgroundAudio.currentTime = 0;
        clickSfxIndex = e;
        sfxAudio.currentTime = 0;
        sfxAudio.play();
        sfxAudio.volume = e * 0.1;
    }


    const drawVolume = (array, indexVolume, clickIndex, handleChangeVolume = () => { }) => {
        array.forEach((element, indexElement) => {
            // console.log(element)
            isClick = pjs.touchControl.isPeekStatic(element.getStaticBox());

            if (indexElement <= indexVolume) {
                element.fillColor = '#2c3c82'
            }

            if (isClick) {
                handleChangeVolume(indexElement);
                if (element.fillColor === '#2c3c82') {
                    element.fillColor = '#9ca3bf'
                } else {
                    element.fillColor = '#2c3c82'
                }
            }

            if (indexElement > clickIndex) {
                element.fillColor = '#9ca3bf'
            }

            if (indexElement === 0) {
                element.fillColor = 'red'
            }
            element.draw();
        });
    }

    pjs.game.newLoop("myGame", function () {
        // Очистка прошлого кадра отрисовки
        pjs.game.clear();


        let indexSoundVolume = defaultMusicVolume;
        let indexSfxVolume = defaultSfxVolume;
        arraySoundBar.forEach((element, indexElement) => {
            if (element.fillColor === '#ffaaff' && indexSoundVolume < indexElement) {
                indexSoundVolume = indexElement;
            }
        });

        if (indexSoundVolume < 0) {
            indexSoundVolume = defaultMusicVolume;
        }

        arraySfxBar.forEach((element, indexElement) => {
            if (element.fillColor === '#ffaaff' && indexSfxVolume < indexElement) {
                indexSfxVolume = indexElement;
            }
        });


        if (indexSfxVolume < 0) {
            indexSfxVolume = defaultSfxVolume;
        }

        drawVolume(arraySoundBar, indexSoundVolume, clickSoundIndex, onChangeVolume);
        drawVolume(arraySfxBar, indexSfxVolume, clickSfxIndex, onChangeSfxVolume);


        borederShip.draw();

        arrayPlayerShip.forEach((playerShip, index) => {
            let isClick = pjs.touchControl.isPeekStatic(playerShip.getStaticBox());

            playerShip.file = `image/playership_${index + 1}.png`

            if (isClick) {
                clickShipIndex = index;
                localStorage.setItem('savedIndexShip', index);
            }

            if (clickShipIndex === index) {
                borederShip.x = playerShip.x
            }

            playerShip.draw();
            // console.log(`isClick`, isClick)
        })

        okBtn.draw();

        let handdleOk = pjs.touchControl.isPeekStatic(okBtn.getStaticBox());

        // console.log(`handdleOk`, handdleOk)

        if (handdleOk) {
            window.location.replace('./index.html')
        }

        pjs.brush.drawText({
            text: 'settings',
            color: "#2c3c82",
            size: 42,
            font: 'Origami',
            align: 'center',
            x: game.getWH().w / 2,
            y: 5
        });

        pjs.brush.drawText({
            text: 'Music:',
            color: "#2c3c82",
            size: 32,
            font: 'Origami',
            align: 'left',
            x: 15,
            y: 80
        });

        pjs.brush.drawText({
            text: 'SFX:',
            color: "#2c3c82",
            size: 32,
            font: 'Origami',
            align: 'left',
            x: 15,
            y: 160
        });
    });


    pjs.game.startLoop("myGame");
}
